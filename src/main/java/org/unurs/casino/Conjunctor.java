/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.unurs.casino;

/**
 *
 * @author redu
 */
public enum Conjunctor {
    OR(" OR "), AND(" AND ");
    
    private final String type;
    
    Conjunctor(String type) {
        this.type=type;
    }

    public String getType() {
        return type;
    }
}
