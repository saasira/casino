/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.unurs.casino;

import java.io.IOException;

/**
 *
 * @author redu
 */
public class CassandraDatabaseException extends IOException {

    public CassandraDatabaseException(String message) {
        super(message);
    }

    public CassandraDatabaseException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
