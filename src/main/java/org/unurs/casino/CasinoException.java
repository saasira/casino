/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unurs.casino;

/**
 *
 * @author Samba
 */
public class CasinoException extends Exception {

    public CasinoException(String message) {
        super(message);
    }

    public CasinoException(String message, Throwable cause) {
        super(message, cause);
    }

    public CasinoException(Throwable cause) {
        super(cause);
    }
    
}
