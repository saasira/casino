/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.unurs.casino;

/**
 *
 * @author Saasira
 * @param <T>
 */
public class Expression<T> {
    private final String left;
    private final T right;
    private final Operator operator;
    private Conjunctor conjunctor;

    public Expression(String left, T right, Operator operator) {
        this.left = left;
        this.right = right;
        this.operator = operator;
    }

    public Conjunctor getConjunctor() {
        return conjunctor;
    }

    protected void setConjunctor(Conjunctor conjunctor) {
        this.conjunctor = conjunctor;
    }

    public String getLeft() {
        return left;
    }

    public Operator getOperator() {
        return operator;
    }

    public T getRight() {
        return right;
    }
}