/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.unurs.casino;

import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author redu
 */
public class Table implements Serializable {
    private static final long serialVersionUID = 859764546587469465L;
    
    private String schema;
    private String name;
    private List<Column> columns=new ArrayList<>();
    private List<String> primaryKey;
    private boolean withOptions;
    private String comment;
    private boolean replicateOnWrite;//replicate_on_write  
    private Float readRepairChance;//read_repair_chance; The probability with which to query extra nodes for the purpose of read repairs.
    private Short DCLocalReadRepairChance;//dclocal_read_repair_chance;The probability with which to query extra nodes belonging to the same data center than the read coordinator for the purpose of read repairs.  
    private Integer GCGraceSeconds;//gc_grace_seconds ;Time to wait before garbage collecting tombstones (deletion markers). 
    private Float bloomFilterFPChance;//bloom_filter_fp_chance;The target probability of false positive of the sstable bloom filters.
    private Map<String,Object> compaction;
    private Map<String,Object> compression;
    private String caching;//Whether to cache keys (“key cache”) and/or rows (“row cache”) for this table. Valid values are: all, keys_only, rows_only and none. 
    private Map<String,String> indices;//Cassandra does not support secondary indexes on multiple columns
    
    public Table(String schema, String name) {
        this.schema = schema;
        this.name = name;
    }

    public String getSchema() {
        return this.schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Column> getColumns() {
        return this.columns;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }
    
    public void addColumns(List<Column> cls) {
        this.columns.addAll(cls);
    }
    
    public boolean addColumn(Column column) {
        boolean present = false;
        if (this.columns.contains(column)) {
            present = true;
        }
        if (!present) {
            this.columns.add(column);
        }
        return present;
    }

    public List<String> getPrimaryKey() {
        return this.primaryKey;
    }

    /**
     * if you want to composite primary key spanning multiple columns
     * @param primaryKey list of columns making up the primary key
     **/
    public void setPrimaryKey(List<String> primaryKey) {
        this.primaryKey = primaryKey;
    }
    
    public boolean hasPrimaryKey() {
        return this.primaryKey!=null;
    }

    public Map<String, String> getIndices() {
        return indices;
    }

    public void setIndices(Map<String, String> indices) {
        this.indices = indices;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
        withOptions=true;
    }

    public boolean isReplicateOnWrite() {
        return this.replicateOnWrite;
    }

    public void setReplicateOnWrite(boolean replicateOnWrite) {
        this.replicateOnWrite = replicateOnWrite;
        withOptions=true;
    }

    public Float getReadRepairChance() {
        return this.readRepairChance;
    }

    public void setReadRepairChance(Float readRepairChance) {
        this.readRepairChance = readRepairChance;
        withOptions=true;
    }

    public Short getDCLocalReadRepairChance() {
        return this.DCLocalReadRepairChance;
    }

    public void setDCLocalReadRepairChance(Short DCLocalReadRepairChance) {
        this.DCLocalReadRepairChance = DCLocalReadRepairChance;
        withOptions=true;
    }

    public Integer getGCGraceSeconds() {
        return this.GCGraceSeconds;
    }

    public void setGCGraceSeconds(Integer GCGraceSeconds) {
        this.GCGraceSeconds = GCGraceSeconds;
        withOptions=true;
    }

    public Float getBloomFilterFPChance() {
        return bloomFilterFPChance;
    }

    public void setBloomFilterFPChance(Float bloomFilterFPChance) {
        this.bloomFilterFPChance = bloomFilterFPChance;
        withOptions=true;
    }

    public Map<String, Object> getCompaction() {
        return this.compaction;
    }

    public void setCompaction(Map<String, Object> compaction) {
        this.compaction = compaction;
        withOptions=true;
    }

    public Map<String, Object> getCompression() {
        return this.compression;
    }

    public void setCompression(Map<String, Object> compression) {
        this.compression = compression;
        withOptions=true;
    }

    public String getCaching() {
        return this.caching;
    }

    public void setCaching(String caching) {
        this.caching = caching;
        withOptions=true;
    }

    public boolean isWithOptions() {
        return withOptions;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.schema);
        hash = 23 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Table other = (Table) obj;
        if (!Objects.equals(this.schema, other.schema)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }
    
    // need to check if we need special handling of serialization;
    // if not, then the following two methods are not required.
    private synchronized void writeObject(ObjectOutputStream oos)
            throws java.io.IOException {
        // Write out element count, and any hidden stuff
        oos.defaultWriteObject();
        int size = columns.size();
        // Write out array length
        oos.writeInt(size);

        // Write out all elements in the proper order.
        for (int i = 0; i < size; i++) {
            oos.writeObject(columns.get(i));
        }
    }

    private void readObject(java.io.ObjectInputStream ois)
            throws java.io.IOException, ClassNotFoundException {
        // Read in size, and any hidden stuff
        ois.defaultReadObject();

        // Read in array length and allocate array
        int size = ois.readInt();
        List<Column> data = new ArrayList<>(size);

        // Read in all elements in the proper order.
        for (int i = 0; i < size; i++) {
            data.add((Column) ois.readObject());
        }
        this.columns = data;
    }

}
