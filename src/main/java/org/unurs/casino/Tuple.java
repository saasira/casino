/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unurs.casino;

import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Core
 */
public final class Tuple implements Serializable {

    private static final long serialVersionUID = 17978465456458412L;

    private String schema;//keyspace
    private String table;//table
    private Map<String,Object> fields=new HashMap<>();

    public Tuple() {
    }

    public Tuple(String schema, String table) {
        this.schema = schema;
        this.table = table;
    }
    
    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public Map<String,Object> getFields() {
        return fields;
    }

    /**
     * overrides the entire existing set of fields in this record; if you want to 
     * merge then use addFields instead
     * @param fields 
     **/
    protected void setFields(Map<String,Object> fields) {
        this.fields = fields;
    }

    public <T> T get(String name) {
        T value=(T)fields.get(name);
        return value;
    }
    
    public <T> void set(String name, T value) {
        fields.put(name, value);
    }
    
    public <T> T unset(String name) {
        return (T)fields.remove(name);
    }
    
    public <T> T update(String name, T value) {
        return (T) fields.put(name,value);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tuple other = (Tuple) obj;
        if ((this.table == null) ? (other.table != null) : !this.table.equals(other.table)) {
            return false;
        }
        return this.fields == other.fields || (this.fields != null && this.fields.equals(other.fields));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + (this.table != null ? this.table.hashCode() : 0);
        hash = 83 * hash + (this.fields != null ? this.fields.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return table.concat(" : ").concat(fields.toString());
    }
}
