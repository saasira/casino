/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.unurs.casino;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author redu
 */
public class Criteria {
    
    private final String schemaName;
    private final String tableName;
    
    private List<String> columns;
    
    private final LinkedList<Expression> expressions=new LinkedList<>();
    
    private short conditions;

    private Criteria(String schemaName, String tableName) {
        this.schemaName = schemaName;
        this.tableName = tableName;
    }
    
    public static Criteria instance(String schemaName, String tableName) {
        return new Criteria(schemaName,tableName);
    }
    
    public Criteria fetch(String... columns) {
        this.columns=Arrays.asList(columns);
        return this;
    }
    
    public Criteria where(Expression expression) {
        expressions.add(expression);
        conditions++;
        return this;
    }
    
    public Criteria and(Expression expression) {
        expressions.get(conditions-1).setConjunctor(Conjunctor.AND);
        expressions.add(expression);
        conditions++;
        return this;
    }
    
    public Criteria or(Expression expression) {
        expressions.get(conditions-1).setConjunctor(Conjunctor.OR);
        expressions.add(expression);
        conditions++;
        return this;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public List<String> getColumns() {
        return columns;
    }

    public LinkedList<Expression> getExpressions() {
        return expressions;
    }
}
