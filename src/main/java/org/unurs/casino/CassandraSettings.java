/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.unurs.casino;

import com.datastax.driver.core.AuthProvider;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.MetricsOptions;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.ProtocolOptions;
import com.datastax.driver.core.ProtocolOptions.Compression;
import com.datastax.driver.core.QueryOptions;
import com.datastax.driver.core.SSLOptions;
import com.datastax.driver.core.SocketOptions;
import com.datastax.driver.core.policies.LoadBalancingPolicy;
import com.datastax.driver.core.policies.ReconnectionPolicy;
import com.datastax.driver.core.policies.RetryPolicy;
import java.net.InetAddress;
import java.util.Set;

/**
 *
 * @author redu
 */
public class CassandraSettings {
    private static volatile String clusterName;//cluster clusterName
    private static volatile Set<String> clusterNodes;//cluster clusterNodes
    private static volatile int clusterPort;//cluste listening clusterPort
    private static volatile String schemaName;
    
    private static volatile String username;//username to connect to the cluster
    private static volatile String password;//password of the user to connect to the cluster
    private static volatile AuthProvider authenticationProvider;
    
    private static volatile boolean SSL;
    private static volatile SSLOptions SSLOptions;
    private static volatile Compression compression;
    
    private static volatile SocketOptions socketOptions;
    private static volatile PoolingOptions poolingOptions;
    private static volatile MetricsOptions metricsOptions;
    private static volatile QueryOptions queryOptions;
    private static volatile RetryPolicy retryPolicy;
    private static volatile ReconnectionPolicy reconnectionPolicy;
    private static volatile LoadBalancingPolicy loadBalancingPolicy;
    private static volatile ConsistencyLevel consistencyLevel;
    
    public static String getClusterName() {
        return clusterName;
    }

    public static void setClusterName(String clusterName) {
        CassandraSettings.clusterName = clusterName;
    }

    public static String getSchemaName() {
        return schemaName;
    }

    public static void setSchemaName(String schemaName) {
        CassandraSettings.schemaName = schemaName;
    }

    public static Set<String> getClusterNodes() {
        return clusterNodes;
    }

    public static void setClusterNodes(Set<String> clusterNodes) {
        CassandraSettings.clusterNodes = clusterNodes;
    }

    public static int getClusterPort() {
        return clusterPort;
    }

    public static void setClusterPort(int clusterPort) {
        CassandraSettings.clusterPort = clusterPort;
    }

    public static ConsistencyLevel getConsistencyLevel() {
        return consistencyLevel;
    }

    public static void setConsistencyLevel(ConsistencyLevel consistencyLevel) {
        CassandraSettings.consistencyLevel = consistencyLevel;
    }

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        CassandraSettings.username = username;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        CassandraSettings.password = password;
    }

    public static AuthProvider getAuthenticationProvider() {
        return authenticationProvider;
    }

    public static void setAuthenticationProvider(AuthProvider authenticationProvider) {
        CassandraSettings.authenticationProvider = authenticationProvider;
    }

    public static boolean isSSL() {
        return SSL;
    }

    public static void setSSL(boolean SSL) {
        CassandraSettings.SSL = SSL;
    }

    public static SSLOptions getSSLOptions() {
        return SSLOptions;
    }

    public static void setSSLOptions(SSLOptions SSLOptions) {
        CassandraSettings.SSLOptions = SSLOptions;
    }

    public static Compression getCompression() {
        return compression;
    }

    public static void setCompression(Compression compression) {
        CassandraSettings.compression = compression;
    }

    public static SocketOptions getSocketOptions() {
        return socketOptions;
    }

    public static void setSocketOptions(SocketOptions socketOptions) {
        CassandraSettings.socketOptions = socketOptions;
    }

    public static PoolingOptions getPoolingOptions() {
        return poolingOptions;
    }

    public static void setPoolingOptions(PoolingOptions poolingOptions) {
        CassandraSettings.poolingOptions = poolingOptions;
    }

    public static MetricsOptions getMetricsOptions() {
        return metricsOptions;
    }

    public static void setMetricsOptions(MetricsOptions metricsOptions) {
        CassandraSettings.metricsOptions = metricsOptions;
    }

    public static QueryOptions getQueryOptions() {
        return queryOptions;
    }

    public static void setQueryOptions(QueryOptions queryOptions) {
        CassandraSettings.queryOptions = queryOptions;
    }

    public static RetryPolicy getRetryPolicy() {
        return retryPolicy;
    }

    public static void setRetryPolicy(RetryPolicy retryPolicy) {
        CassandraSettings.retryPolicy = retryPolicy;
    }

    public static ReconnectionPolicy getReconnectionPolicy() {
        return reconnectionPolicy;
    }

    public static void setReconnectionPolicy(ReconnectionPolicy reconnectionPolicy) {
        CassandraSettings.reconnectionPolicy = reconnectionPolicy;
    }

    public static LoadBalancingPolicy getLoadBalancingPolicy() {
        return loadBalancingPolicy;
    }

    public static void setLoadBalancingPolicy(LoadBalancingPolicy loadBalancingPolicy) {
        CassandraSettings.loadBalancingPolicy = loadBalancingPolicy;
    }
}
