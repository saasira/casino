/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unurs.casino;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Samba
 * @param <T>
 */
public abstract class ORM<T extends ORM> {
    
    public abstract T transform(Row row);
    
    public abstract Tuple transform();
    
    public T one(ResultSet rs) {
        T type=null;
        Row row=rs.one();
        if(row!=null) {
            type=ORM.this.transform(row);
        }
        return type;
    }
    
    public List<T> all(ResultSet rs) {
        List<T> types=new ArrayList<>();
        for(Row row : rs) {
            types.add(ORM.this.transform(row));
        }
        return types;
    }
    
    public Iterator<T> iterator(ResultSet rs) {
        final Iterator<Row> rows = rs.iterator();
        Iterator<T> types = new Iterator<T>() {

            @Override
            public boolean hasNext() {
                return rows.hasNext();
            }

            @Override
            public T next() {
                return ORM.this.transform(rows.next());
            }
        
        };
        return types;
    }
}
