/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.unurs.casino;

import java.util.Objects;

/**
 *
 * @author redu
 */
public class Operator {
    private String command;
    private static final String EQUALS="=";
    private static final String NOT_EQUALS="<>";
    private static final String LESS_THAN="<";
    private static final String GREATER_THAN=">";
    private static final String LESS_THAN_OR_EQUAL="<=";
    private static final String GREATER_THAN_OR_EQUAL=">=";
    
    private Operator(String command){
        this.command=command;
    }
    
    public static Operator eq(){
        return new Operator(EQUALS);
    }
    
    public static Operator neq(){
        return new Operator(NOT_EQUALS);
    }
    
    public static Operator lt(){
        return new Operator(LESS_THAN);
    }
    
    public static Operator gt(){
        return new Operator(GREATER_THAN);
    }
    
    public static Operator lte(){
        return new Operator(LESS_THAN_OR_EQUAL);
    }
    
    public static Operator gte(){
        return new Operator(GREATER_THAN_OR_EQUAL);
    }

    @Override
    public String toString() {
        return this.command;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.command);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Operator other = (Operator) obj;
        return Objects.equals(this.command, other.command);
    }
    
    
}
