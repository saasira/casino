/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.unurs.casino;

import com.datastax.driver.core.AuthProvider;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.CloseFuture;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ColumnDefinitions;
import com.datastax.driver.core.ColumnDefinitions.Definition;
import com.datastax.driver.core.Configuration;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.DataType;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.KeyspaceMetadata;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.MetricsOptions;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ProtocolOptions;
import com.datastax.driver.core.QueryOptions;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.SSLOptions;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.SocketOptions;
import com.datastax.driver.core.TableMetadata;
import com.datastax.driver.core.policies.LoadBalancingPolicy;
import com.datastax.driver.core.policies.Policies;
import com.datastax.driver.core.policies.ReconnectionPolicy;
import com.datastax.driver.core.policies.RetryPolicy;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.UUID;
import java.net.InetAddress;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Date;
import java.nio.ByteBuffer;
import java.math.BigInteger;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import org.unurs.linker.Constant;
import org.unurs.linker.Formatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author redu
 */
public final class Cassandra {
    private static final Logger logger=LoggerFactory.getLogger(Cassandra.class);
    private static volatile Cluster cluster;
    private static volatile Session session;
    private static volatile boolean closed;
    private static final Map<String,BoundStatement> namedQueries=new ConcurrentHashMap<>();
    public static void connect() {
        Cluster.Builder builder= Cluster.builder();
        String clusterName=CassandraSettings.getClusterName();
        if(clusterName!=null) {
            builder.withClusterName(clusterName);
        }
        
        int clusterPort = CassandraSettings.getClusterPort();
        if(clusterPort>0) {
            builder.withPort(clusterPort);
        }
        
        String username=CassandraSettings.getUsername();
        String password=CassandraSettings.getPassword();
        if(username!=null && password!=null) {
            builder.withCredentials(username, password);
        }
        AuthProvider authentiationProvider=CassandraSettings.getAuthenticationProvider();
        if(authentiationProvider!=null) {
            builder.withAuthProvider(authentiationProvider);
        }
        
        Iterator<String> iterator=CassandraSettings.getClusterNodes().iterator();
        String node;
        while(iterator.hasNext()) {
            node=iterator.next();
            builder.addContactPoint(node);
        }
        
        SocketOptions socketOptions=CassandraSettings.getSocketOptions();
        if(socketOptions!=null) {
            builder.withSocketOptions(socketOptions);
        }
        
        PoolingOptions poolingOptions=CassandraSettings.getPoolingOptions();
        if(poolingOptions!=null) {
            builder.withPoolingOptions(poolingOptions);
        }
        
        QueryOptions queryOptions=CassandraSettings.getQueryOptions();
        if(queryOptions!=null) {
            builder.withQueryOptions(queryOptions);
        }
        
        MetricsOptions metricsOptions=CassandraSettings.getMetricsOptions();
        if(metricsOptions!=null) {
            if(!metricsOptions.isJMXReportingEnabled()){
                builder.withoutJMXReporting();
            }
        }
        
        LoadBalancingPolicy lbp=CassandraSettings.getLoadBalancingPolicy();
        if(lbp!=null) {
            builder.withLoadBalancingPolicy(lbp);
        }
        ReconnectionPolicy rcp=CassandraSettings.getReconnectionPolicy();
        if(rcp!=null) {
            builder.withReconnectionPolicy(rcp);
        }
        RetryPolicy rp=CassandraSettings.getRetryPolicy();
        if(rp!=null) {
            builder.withRetryPolicy(rp);
        }
        
        if(CassandraSettings.isSSL()) {
            builder.withSSL();
            SSLOptions sslOptions=CassandraSettings.getSSLOptions();
            if(sslOptions!=null) {
                builder.withSSL(sslOptions);
            }
        }
        synchronized(Cassandra.class) {
            if(cluster==null) {
                cluster=builder.build();
            }
        }
    }

    protected static Cluster getCluster() {
        if(cluster!=null && !closed) {
            return cluster;
        }
        synchronized(Cassandra.class) {
            if(cluster!=null && !closed) {
                return cluster;
            }
            Cassandra.connect();
        }
        return cluster;
    }
    
    public static Metadata getClusterMetadata() {
        Metadata metadata = getCluster().getMetadata();
        logger.debug("Connected to cluster: %s\n",metadata.getClusterName());
        for (Host host : metadata.getAllHosts()) {
            logger.debug(Formatter.format("Datacenter: {0}; Rack: {1}; Host: {2}\n", host.getDatacenter(), host.getRack(), host.getAddress()));
        }
        return metadata;
    }
    
    public static Session getSession() {
        if (session != null && !closed) {
            return session;
        }
        synchronized(Cassandra.class) {
            if (session != null && !closed) {
                return session;
            }
            String schemaName=CassandraSettings.getSchemaName();
            if(schemaName!=null) {
                session = getCluster().connect(schemaName);
            } else {
                session=getCluster().connect();
            }
        }
        return session;
    }
    
    public static boolean doesSchemaExist(String schemaName) throws CassandraDatabaseException {
        boolean exists=false;
        Metadata metadata;
        KeyspaceMetadata km;
        try {
            metadata= getCluster().getMetadata();
            km=metadata.getKeyspace(schemaName);
        } catch(Exception exception) {
            throw new CassandraDatabaseException(Formatter.format("error checking for existance of schema {0}", schemaName),exception);
        }
        if(km!=null) {
            exists=true;
        }
        return exists;
    }
    
    public static boolean doesTableExist(String schemaName, String tableName) throws CassandraDatabaseException {
        boolean exists=false;
        Metadata metadata;
        KeyspaceMetadata km=null;
        TableMetadata tm;
        try {
            metadata = getCluster().getMetadata();
            km = metadata.getKeyspace(schemaName);
            if(km!=null) {
                tm=km.getTable(tableName);
                if(tm!=null) {
                    exists=true;
                }
            }
        } catch(Exception exception) {
            throw new CassandraDatabaseException(Formatter.format("error checking for existance of table {0} in {1}",tableName,schemaName),exception);
        }
        
        return exists;
    }
    
    public static void createSchema(String name, String replicationStrategy, int replicationFactor, boolean durableWrites) { 
        StringBuilder builder=new StringBuilder("CREATE KEYSPACE IF NOT EXISTS ");
        //builder.append(Constant.SPACE);
        builder.append(name);
        builder.append(Constant.SPACE);
        boolean replication=false;
        if(replicationStrategy!=null && replicationFactor!=0) {
            replication=true;
            builder.append("WITH replication");
            //builder.append(Constant.SPACE);
            builder.append("= {'class' : ");// is space required after equals sign?
            builder.append("'");
            builder.append(replicationStrategy);
            builder.append("'");
            builder.append(",");
            builder.append("'replication_factor' : ");
            builder.append(replicationFactor);
            builder.append(Constant.SPACE);
            builder.append("}");
        }
        if(durableWrites) {
            if(replication) {
                builder.append(" AND ");
            } else {
                builder.append(" WITH ");
            } 
            builder.append("durable_writes = ")
                    .append(durableWrites);
        }
        builder.append(Constant.SEMICOLON);
        
        getSession().execute(builder.toString());
    }
    
    public static void createTable(Table table) {
        StringBuilder builder=new StringBuilder("CREATE TABLE IF NOT EXISTS ");
        builder.append(table.getSchema())
                .append(Constant.DOT)
                .append(table.getName())
                .append(Constant.SPACE)
                .append(Constant.LEFT_PARENTHESIS);
        
        List<Column> columns=table.getColumns();
        Iterator<Column> iterator=columns.iterator();
        Column column;
        while(iterator.hasNext()) {
            column=iterator.next();
            builder.append(column.getName());
            builder.append(Constant.SPACE);
            builder.append(column.getType().getName().toString());
            builder.append(Constant.SPACE);
            if(column.isPrimaryKey()) {
                builder.append("PRIMARY KEY");
            }
            if(iterator.hasNext() || table.hasPrimaryKey()) {
                builder.append(Constant.COMMA);
            }
        }
        
        if(table.hasPrimaryKey()) {
            builder.append("PRIMARY KEY");
            builder.append(Constant.LEFT_PARENTHESIS);
                    
            List<String> pk=table.getPrimaryKey();
            Iterator<String> pki=pk.iterator();
            String pkc;
            while(pki.hasNext()) {
                pkc=pki.next();
                builder.append(pkc);
                if(pki.hasNext()) {
                    builder.append(Constant.COMMA);
                }
            }
            builder.append(Constant.RIGHT_PARENTHESIS);
        }
        builder.append(Constant.RIGHT_PARENTHESIS);
        
        if(table.isWithOptions()) {
            builder.append(" WITH ");
        }
        
        boolean options=false;
        
        String comment=table.getComment();
        if(comment!=null) {
            builder.append(" comment = ")
                    .append(Constant.SINGLE_QUOTE)
                    .append(comment)
                    .append(Constant.SINGLE_QUOTE);
            options=true;
        }
        
        if(table.isReplicateOnWrite()) {
            if(options) {
                builder.append(" AND ");
            }
            builder.append(" replicate_on_write = ")
                    .append(" true ");
            options=true;
        }
        
        if(table.getReadRepairChance()!=null) {
            if(options) {
                builder.append(" AND ");
            }
            builder.append(" read_repair_chance = ")
                    .append(table.getReadRepairChance())
                    .append(Constant.SPACE);
            options=true;
        }
        
        if(table.getBloomFilterFPChance()!=null) {
            if(options) {
                builder.append(" AND ");
            }
            builder.append(" bloom_filter_fp_chance = ")
                    .append(table.getBloomFilterFPChance())
                    .append(Constant.SPACE);
            options=true;
        }
        
        if(table.getDCLocalReadRepairChance()!=null) {
            if(options) {
                builder.append(" AND ");
            }
            builder.append(" dclocal_read_repair_chance = ")
                    .append(table.getDCLocalReadRepairChance())
                    .append(Constant.SPACE);
            options=true;
        }
        
        if(table.getGCGraceSeconds()!=null) {
            if(options) {
                builder.append(" AND ");
            }
            builder.append(" gc_grace_seconds = ")
                    .append(table.getGCGraceSeconds())
                    .append(Constant.SPACE);
            options=true;
        }
        
        if(table.getCompaction()!=null) {
            if(options) {
                builder.append(" AND ");
            }
            builder.append(" compaction = ");
            
            Map<String,Object> compaction=table.getCompaction();
            Iterator<Map.Entry<String,Object>> compactionOptionsIterator=compaction.entrySet().iterator();
            Map.Entry<String,Object> option;
            String key;
            Object value;
            
            builder.append(Constant.LEFT_BRACE);
            
            while(compactionOptionsIterator.hasNext()) {
                option=compactionOptionsIterator.next();
                key=option.getKey();
                value=option.getValue();
                builder.append(Constant.SINGLE_QUOTE)
                        .append(key)
                        .append(Constant.SINGLE_QUOTE)
                        .append(Constant.COLON);
                if(value instanceof String) {
                    builder.append(Constant.SINGLE_QUOTE)
                            .append(value)
                            .append(Constant.SINGLE_QUOTE);
                } else {
                    builder.append(value);
                }
                
                if(compactionOptionsIterator.hasNext()) {
                    builder.append(Constant.COMMA);
                } else {
                    builder.append(Constant.RIGHT_BRACE);
                }
            }
            options=true;
        }
        
        if(table.getCompression()!=null) {
            if(options) {
                builder.append(" AND ");
            }
            builder.append(" compression = ");
            
            Map<String,Object> compression=table.getCompression();
            Iterator<Map.Entry<String,Object>> compressionOptionsIterator=compression.entrySet().iterator();
            Map.Entry<String,Object> option;
            String key;
            Object value;
            
            builder.append(Constant.LEFT_BRACE);
            
            while(compressionOptionsIterator.hasNext()) {
                option=compressionOptionsIterator.next();
                key=option.getKey();
                value=option.getValue();
                builder.append(Constant.SINGLE_QUOTE)
                        .append(key)
                        .append(Constant.SINGLE_QUOTE)
                        .append(Constant.SPACE)
                        .append(Constant.COLON)
                        .append(Constant.SPACE);
                if(value instanceof String) {
                    builder.append(Constant.SINGLE_QUOTE)
                            .append(value)
                            .append(Constant.SINGLE_QUOTE);
                } else {
                    builder.append(value);
                }
                
                if(compressionOptionsIterator.hasNext()) {
                    builder.append(Constant.COMMA);
                } else {
                    builder.append(Constant.RIGHT_BRACE);
                }
            }
            options=true;
        }
        
        if(table.getCaching()!=null) {
            if(options) {
                builder.append(" AND ");
            }
            builder.append(" caching = ")
                    .append(table.getCaching());
        }
        
        builder.append(Constant.SEMICOLON);
        getSession().execute(builder.toString());
        builder.setLength(0);
            
        Map<String,String> indices=table.getIndices();
        if(indices!=null && !indices.isEmpty()) {
            Iterator<Map.Entry<String,String>> indicesIterator=indices.entrySet().iterator();
            Map.Entry<String,String> entry;
            while(indicesIterator.hasNext()) {
                entry=indicesIterator.next();
                String indexName=entry.getKey();
                String indexColumn=entry.getValue();
                builder.append("CREATE INDEX ")
                        .append(" IF NOT EXISTS ")
                        .append(indexName)
                        .append(" ON ")
                        .append(table.getSchema())
                        .append(Constant.DOT)
                        .append(table.getName())
                        .append(Constant.LEFT_PARENTHESIS)
                        .append(indexColumn)
                        .append(Constant.RIGHT_PARENTHESIS)
                        .append(Constant.SEMICOLON);
                
                getSession().execute(builder.toString());//we create the index synchronously as the table is empty
            }
            
        }
    }
    
    public static ResultSetFuture createIndex(String schemaName,String tableName, String indexName, String indexColumn) {
        ResultSetFuture rsf=null;
        StringBuilder builder=new StringBuilder("CREATE INDEX ").append(" IF NOT EXISTS ");
        builder.append(Constant.SPACE).append(indexName).append(Constant.SPACE);
        builder.append(" ON ").append(schemaName).append(Constant.DOT).append(tableName);
        builder.append(Constant.LEFT_PARENTHESIS)
                .append(indexColumn)
                .append(Constant.RIGHT_PARENTHESIS)
                .append(Constant.SEMICOLON);
        rsf=getSession().executeAsync(builder.toString());
        return rsf;
    }
    
    public static ResultSetFuture dropIndex(String tableName, String indexName) {
        ResultSetFuture rsf=null;
        StringBuilder builder=new StringBuilder("DROP INDEX ").append(" IF EXISTS ");
        builder.append(indexName).append(Constant.SEMICOLON);

        rsf=getSession().executeAsync(builder.toString());
        return rsf;
    }
    
    public static void dropTable(String schemaName, String tableName) {
        StringBuilder builder=new StringBuilder("DROP TABLE").append(" IF EXISTS ");
        builder.append(schemaName).append(Constant.DOT);
        builder.append(tableName).append(Constant.SEMICOLON);
        getSession().execute(builder.toString());
    }
    
    public static void insert(Tuple record) {
        StringBuilder builder=transform(record);
        builder.append(Constant.SEMICOLON);
        logger.debug(builder.toString());
        String cql=builder.toString();
        getSession().execute(cql);
    }
    
    public static void prepareQuery(String name,String cql) throws CassandraDatabaseException {
        try {
        PreparedStatement ps = getSession().prepare(cql);
        ps.setConsistencyLevel(CassandraSettings.getConsistencyLevel());
        BoundStatement bs = ps.bind();
        namedQueries.put(name, bs);
        } catch(Exception exception) {
            throw new CassandraDatabaseException(Formatter.format("failed to prepare the query : {0} with CQL : {1} ",name, cql),exception);
        }
    }
    
    public static void executePreparedCommand(String queryName, List<Object> bindParameters) throws CassandraDatabaseException {
        BoundStatement bs=namedQueries.get(queryName);
        if(bs==null) {
            throw new IllegalArgumentException("no prepared query with such name : "+queryName);
        }
        if(bindParameters!=null && !bindParameters.isEmpty()) {
            Object value;
            int size=bindParameters.size();
            for(int index=0;index<size;index++) {
                value=bindParameters.get(index);
                bs=setBoundValue(bs, index, value);
            }
        }
        getSession().execute(bs);
    }
    
    public static void executePreparedCommand(String queryName, Tuple tuple) throws CassandraDatabaseException {
        BoundStatement bs=namedQueries.get(queryName);
        if(bs==null) {
            throw new IllegalArgumentException("no prepared query with such name : "+queryName);
        }
        if(tuple!=null && !tuple.getFields().isEmpty()) {
            Map<String,Object> fields=tuple.getFields();
            Iterator<Map.Entry<String,Object>> iterator=fields.entrySet().iterator();
            Map.Entry<String,Object> entry;
            String key;
            Object value;
            while(iterator.hasNext()) {
                entry = iterator.next();
                key=entry.getKey();
                value=entry.getValue();
                bs=setBoundValue(bs, key, value);
            }
        }
        try {
            getSession().execute(bs);
        } catch(Exception exception) {
            throw new CassandraDatabaseException(Formatter.format("error executing the query : {0}", queryName),exception);
        }
    }
    
    public static void executePreparedBatch(String cql, List<List<Object>> bindParameters) throws CassandraDatabaseException {
        //since the batch statements need to be added to the batch before 
        //preparing the query, this prepared query cannot be cached at application 
        //levels; hence we prepare the query afresh for every batch that comes in
        StringBuilder builder=new StringBuilder("BEGIN BATCH").append(Constant.LINE);
        Iterator<List<Object>> iterator=bindParameters.iterator();
        List record;
        while(iterator.hasNext()) {
            builder.append(cql).append(Constant.LINE);
        }
        builder.append(Constant.SEMICOLON);
        
        PreparedStatement ps = getSession().prepare(builder.toString());
        ps.setConsistencyLevel(CassandraSettings.getConsistencyLevel());
        BoundStatement bs = ps.bind();
        
        iterator=bindParameters.iterator();
        while(iterator.hasNext()) {
            record=iterator.next();
            Object value;
            int size=bindParameters.size();
            for(int index=0;index<size;index++) {
                value=record.get(index);
                bs=setBoundValue(bs, index, value);
            }
        }
        getSession().execute(bs);
    }
    
    public static void executePreparedBatch(String cql, Collection<Tuple> tuples) throws CassandraDatabaseException {
        //since the batch statements need to be added to the batch before 
        //preparing the query, this prepared query cannot be cached at application 
        //levels; hence we prepare the query afresh for every batch that comes in
        StringBuilder builder=new StringBuilder("BEGIN BATCH").append(Constant.LINE);
        Iterator<Tuple> iterator=tuples.iterator();
        Tuple record;
        while(iterator.hasNext()) {
            builder.append(cql).append(Constant.LINE);
        }
        builder.append(Constant.SEMICOLON);
        
        PreparedStatement ps = getSession().prepare(builder.toString());
        ps.setConsistencyLevel(CassandraSettings.getConsistencyLevel());
        BoundStatement bs = ps.bind();
        
        iterator=tuples.iterator();
        while(iterator.hasNext()) {
            record=iterator.next();
            Map<String,Object> fields=record.getFields();
            Iterator<Map.Entry<String,Object>> fieldsIterator=fields.entrySet().iterator();
            Map.Entry<String,Object> entry;
            String key;
            Object value;
            while(fieldsIterator.hasNext()) {
                entry = fieldsIterator.next();
                key=entry.getKey();
                value=entry.getValue();
                bs=setBoundValue(bs, key, value);
            }
        }
        getSession().execute(bs);
    }
    
    public static Tuple querySingle(String name,List<Object> bindParameters) throws CassandraDatabaseException {
        Tuple record=null;
        BoundStatement bs=namedQueries.get(name);
        if(bs==null) {
            throw new IllegalArgumentException("no prepared query with such name : "+name);
        }
        
        Object value;
        int size=bindParameters.size();
        for(int index=0;index<size;index++) {
            value=bindParameters.get(index);
            bs=setBoundValue(bs, index, value);
        }
        ResultSet results=getSession().execute(bs);
        Row row=results.one();
        if(row!=null) {
            record=transform(row,results.getColumnDefinitions());
        }
        return record;
    }
    
    public static List<Tuple> queryMultiple(String name,List<Object> bindParameters) throws CassandraDatabaseException {
        List<Tuple> records=new ArrayList<>();
        BoundStatement bs=namedQueries.get(name);
        if(bs==null) {
            throw new IllegalArgumentException("no prepared query with such name : "+name);
        }
        
        Object value;
        int size=bindParameters.size();
        for(int index=0;index<size;index++) {
            value=bindParameters.get(index);
            bs=setBoundValue(bs, index, value);
        }
        ResultSet results=getSession().execute(bs);
        for(Row row : results) {
            records.add(transform(row, results.getColumnDefinitions()));
        }
        
        return records;
    }
    
    private static BoundStatement bindQueryParameters(String queryName, Map<String,Object> bindParameters) throws CassandraDatabaseException {
        BoundStatement bs=namedQueries.get(queryName);
        if(bs==null) {
            throw new IllegalArgumentException("no prepared query with such name : "+queryName);
        }
        
        if(bindParameters!=null && !bindParameters.isEmpty()) {
            Iterator<Map.Entry<String,Object>> iterator=bindParameters.entrySet().iterator();
            Map.Entry<String,Object> entry;
            String key;
            Object value;
            while(iterator.hasNext()) {
                entry = iterator.next();
                key=entry.getKey();
                value=entry.getValue();
                bs=setBoundValue(bs, key, value);
            }
        }
        return bs;
    }
    
    public static Tuple querySingle(String name, Map<String,Object> bindParameters) throws CassandraDatabaseException {
        Tuple record=null;
        BoundStatement bs=bindQueryParameters(name, bindParameters);
        ResultSet results=getSession().execute(bs);
        Row row=results.one();
        if(row!=null) {
            record=transform(row,results.getColumnDefinitions());
        }
        return record;
    }
    
    public static List<Tuple> queryMultiple(String queryName,Map<String,Object> bindParameters) throws CassandraDatabaseException {
        List<Tuple> records=new ArrayList<>();
        BoundStatement bs=bindQueryParameters(queryName, bindParameters);
        ResultSet results=getSession().execute(bs);
        for(Row row : results) {
            records.add(transform(row, results.getColumnDefinitions()));
        }
        return records;
    }

    public static void insert(List<Tuple> records) {
        StringBuilder builder=new StringBuilder("BEGIN BATCH");
        builder.append(Constant.LINE);
        
        Iterator<Tuple> iterator=records.iterator();
        Tuple record;
        while(iterator.hasNext()) {
            record=iterator.next();
            builder.append(transform(record));
            builder.append(Constant.LINE);
        }
        builder.append("APPLY BATCH;");
        
        getSession().execute(builder.toString());
    }
    
    /**
     * it is important to remember not to set value(s) for primary key column(s); 
     * Cassandra database does not allow changing the  values for primary key 
     * columns after their first insertion 
     * @param record the record with values for the columns that needs to be updated; 
     * schema & table names must be supplied in this record
     **/
    public static void update(Tuple record,String identifierName,Object identifierValue) {
        StringBuilder builder=transformForUpdate(record);
        if(identifierName!=null) {
            builder.append("WHERE ").append(identifierName).append("=");
            Object value=identifierValue;
            if(value instanceof String) {
                builder.append(Constant.SINGLE_QUOTE);
                builder.append(value);
                builder.append(Constant.SINGLE_QUOTE);
            } else {
                builder.append(value);
            }
        }
        builder.append(Constant.SEMICOLON);
        getSession().execute(builder.toString());
    }
    
    public static void update(Tuple record,Map<String,Object> identifiers) {
        StringBuilder builder=transformForUpdate(record);
        if(identifiers!=null) {
            builder.append("WHERE ");
            
            Iterator<Map.Entry<String,Object>> iterators=identifiers.entrySet().iterator();
            Map.Entry<String,Object> identifier;
            while(iterators.hasNext()) {
                identifier=iterators.next();
                builder.append(identifier.getKey()).append("=");
                Object value=identifier.getValue();
                if(value instanceof String) {
                    builder.append(Constant.SINGLE_QUOTE);
                    builder.append(value);
                    builder.append(Constant.SINGLE_QUOTE);
                } else {
                    builder.append(value);
                }
                if(iterators.hasNext()) {
                    builder.append(" AND ");
                }
            }
        }
        builder.append(Constant.SEMICOLON);
        getSession().execute(builder.toString());
    }
    
    public static void delete(String schema, String table, String identifierName,Object identifierValue) {
        StringBuilder builder=new StringBuilder("DELETE FROM ");
        builder.append(schema).append(Constant.DOT);
        builder.append(table).append(Constant.SPACE);
        builder.append("WHERE ").append(identifierName).append("=")
                .append(identifierValue);
        getSession().execute(builder.toString());
    }
    
    public static Tuple find(String schema,String table, Expression expression) {
        return find(schema,table,expression,false);
    }
    
    public static Tuple find(String schema,String table,Expression expression,boolean allowFiltering) {
        Tuple record;
        StringBuilder builder=new StringBuilder("SELECT * FROM ");
        builder.append(schema).append(Constant.DOT);
        builder.append(table).append(Constant.SPACE);
        builder.append("WHERE").append(Constant.SPACE);
        builder.append(expression.getLeft()).
                append(Constant.SPACE)
                .append(expression.getOperator());
        
        Object value=expression.getRight();
        if(value instanceof String) {
            builder.append(Constant.SINGLE_QUOTE);
            builder.append(value);
            builder.append(Constant.SINGLE_QUOTE);
        } else {
            builder.append(value);
        }
        
        if(allowFiltering) {
            builder.append(" ALLOW FILTERING ");
        }
        builder.append(Constant.SEMICOLON);
        record=querySingle(builder.toString());
        return record;
    }
    
    public static Tuple find(String schema, String table,LinkedList<Expression> identifiers) {
        return find(schema,table,identifiers,false);
    }
    
    public static Tuple find(String schema, String table,LinkedList<Expression> identifiers, boolean allowFiltering) {
        Tuple record;
        StringBuilder builder=new StringBuilder("SELECT * FROM ");
        builder.append(schema).append(Constant.DOT);
        builder.append(table).append(Constant.SPACE);
        builder.append("WHERE ");
        Iterator<Expression>  iterator=identifiers.iterator();
        Expression expression;
        
        while(iterator.hasNext()) {
            expression=iterator.next();
            builder.append(expression.getLeft())
                    .append(Constant.SPACE)
                    .append(expression.getOperator())
                    .append(Constant.SPACE);
            
            Object value=expression.getRight();
            
            builder.append(Constant.SPACE);
            if(value instanceof String) {
                builder.append(Constant.SINGLE_QUOTE);
                builder.append(value);
                builder.append(Constant.SINGLE_QUOTE);
            } else {
                builder.append(value);
            }
            if(iterator.hasNext()) {
                Conjunctor conjunctor=expression.getConjunctor();
                if(conjunctor!=null) {
                    builder.append(Constant.SPACE).append(conjunctor.getType()).append(Constant.SPACE);
                } else {
                    builder.append(Constant.SPACE).append(Conjunctor.OR.getType()).append(Constant.SPACE);
                }
            }
        }
        if(allowFiltering) {
            builder.append(" ALLOW FILTERING ");
        }
        builder.append(Constant.SEMICOLON);
        record=querySingle(builder.toString());
        return record;
    }
    
    public static Tuple querySingle(String cql) {
        Tuple record=null;
        ResultSet results=getSession().execute(cql);
        Row row=results.one();
        if(row!=null) {
            record=transform(row,results.getColumnDefinitions());
        }
        return record;
    }
    
    public static List<Tuple> queryMultiple(String cql) {
        List<Tuple> records=new ArrayList<>();
        ResultSet results=getSession().execute(cql);
        for(Row row : results) {
            records.add(transform(row, results.getColumnDefinitions()));
        }
        return records;
    }
    
    public <T extends ORM> T querySingle(String queryName, Map<String,Object> bindParameters, Class<? extends ORM> klass) throws CassandraDatabaseException, CasinoException {
        T type=null;
        BoundStatement bs=bindQueryParameters(queryName, bindParameters);
        ResultSet results=getSession().execute(bs);
        ORM<T> orm;
        try {
            orm = klass.newInstance();
            type=orm.one(results);
        } catch (InstantiationException | IllegalAccessException ex) {
            String message = Formatter.format("failed to instantiate the concrete ORM class : ",klass.getName());
            logger.error(message, ex);
            throw new CasinoException(message,ex);
        }
        return type;
    }
    
    public <T extends ORM> List<T> queryMultiple(String queryName, Map<String,Object> bindParameters, Class<? extends ORM> klass) throws CassandraDatabaseException, CasinoException {
        List<T> types=null;
        BoundStatement bs=bindQueryParameters(queryName, bindParameters);
        ResultSet results=getSession().execute(bs);
        ORM<T> orm=null;
        try {
            orm = klass.newInstance();
            types=orm.all(results);
        } catch (InstantiationException | IllegalAccessException ex) {
            String message = Formatter.format("failed to instantiate the concrete ORM class : ",klass.getName());
            logger.error(message, ex);
            throw new CasinoException(message,ex);
        }
        return types;
    }
    
    public <T extends ORM> Iterator<T> iterator(String queryName, Map<String,Object> bindParameters, Class<? extends ORM> klass) throws CassandraDatabaseException, CasinoException {
        Iterator<T> iterator=null;
        BoundStatement bs=bindQueryParameters(queryName, bindParameters);
        ResultSet results=getSession().execute(bs);
         ORM<T> orm=null;
        try {
            orm = klass.newInstance();
            iterator=orm.iterator(results);
        } catch (InstantiationException | IllegalAccessException ex) {
            String message = Formatter.format("failed to instantiate the concrete ORM class : ",klass.getName());
            logger.error(message, ex);
            throw new CasinoException(message,ex);
        }
        return iterator;
    }
    
    public static void dropSchema(String schemaName) {
        StringBuilder builder=new StringBuilder("DROP KEYSPACE ").append("IF EXISTS ");
        builder.append(schemaName);
        builder.append(Constant.SEMICOLON);
        
        getSession().execute(builder.toString());
        
        logger.debug("Finished dropping schema : "+schemaName);
     }
    
    public static void close() {
        getCluster().close();
        closed=true;
    }
    
    public static CloseFuture closeAsync() {
        //session.shutdown();//not needed; the cluster.shutdown() will intrnall call shutdown() method on all sessions
        CloseFuture future =getCluster().closeAsync();
        closed=true;
        return future;
    }
    
    public Iterator<Row> queryMultiple(Criteria criteria) {
        String query=null;
        ResultSet rs=getSession().execute(query);
        return rs.iterator();
    }
    
    public Iterator<Row> query(String queryName, List<Object> bindParameters, int batchSize) throws CassandraDatabaseException {
        BoundStatement bs=namedQueries.get(queryName);
        if(bs==null) {
            throw new IllegalArgumentException("no prepared query with such name : "+queryName);
        }
        if(bindParameters!=null) {
            Object value;
            int size=bindParameters.size();
            for(int index=0;index<size;index++) {
                value=bindParameters.get(index);
                bs=setBoundValue(bs, index, value);
            }
        }
        
        bs.setFetchSize(batchSize);
        ResultSet rs=getSession().execute(bs);
        /*
        final Iterator<Row> rows=rs.iterator();
        Iterator<Tuple> iterator = new Iterator<Tuple>() {
            
            @Override
            public boolean hasNext() {
                return rows.hasNext();
            }

            @Override
            public Tuple next() {
                return transform(rows.next());
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
            
        };
        */
        return rs.iterator();
    }
    
    private static Tuple transform(Row row, ColumnDefinitions definitions) {
        Tuple record=new Tuple();
        
        for(int index=0;index<definitions.size();index++) {
            DataType type=definitions.getType(index);
            Class<?> klass=type.asJavaClass();
            if(klass == String.class) {
                String name=definitions.getName(index);
                String value=row.getString(index);
                record.set(name, value);
            } else if(klass == Long.class) {
                String name=definitions.getName(index);
                Long value=row.getLong(index);
                record.set(name, value);
            } else if(klass == Double.class) {
                String name=definitions.getName(index);
                Double value=row.getDouble(index);
                record.set(name, value);
            } else if(klass == Boolean.class) {
                String name=definitions.getName(index);
                Boolean value=row.getBool(index);
                record.set(name, value);
            } else if(klass == Integer.class) {
                String name=definitions.getName(index);
                Integer value=row.getInt(index);
                record.set(name, value);
            } else if(klass == Float.class) {
                String name=definitions.getName(index);
                Float value=row.getFloat(index);
                record.set(name, value);
            } else if(klass == Date.class) {
                String name=definitions.getName(index);
                Date value=row.getDate(index);
                record.set(name, value);
            } else if(klass == BigInteger.class) {
                String name=definitions.getName(index);
                BigInteger value=row.getVarint(index);
                record.set(name, value);
            } else if(klass == BigDecimal.class) {
                String name=definitions.getName(index);
                BigDecimal value=row.getDecimal(index);
                record.set(name, value);
            } else if(klass == UUID.class) {
                String name=definitions.getName(index);
                //ByteBuffer data = row.getBytesUnsafe("uid");
                //UUID uuid = new UUID(data.getLong(), data.getLong());
                UUID value=row.getUUID(index);
                record.set(name, value);
            } else if(klass == InetAddress.class) {
                String name=definitions.getName(index);
                InetAddress value=row.getInet(index);
                record.set(name, value);
            } else if(klass == ByteBuffer.class) {
                String name=definitions.getName(index);
                ByteBuffer value=row.getBytes(index);
                record.set(name, value);
            } else if(klass == Set.class) {
                TypeVariable<Class<?>>[] typeVariable= (TypeVariable<Class<?>>[]) klass.getTypeParameters();
                Class<?> elementClass=typeVariable[0].getGenericDeclaration();
                
                String name=definitions.getName(index);
                Set value=row.getSet(index,elementClass);
                record.set(name, value);
            } else if(klass == List.class) {
                TypeVariable<Class<?>>[] typeVariable= (TypeVariable<Class<?>>[]) klass.getTypeParameters();
                Class<?> elementClass=typeVariable[0].getGenericDeclaration();
                
                String name=definitions.getName(index);
                List value=row.getList(index,elementClass);
                record.set(name, value);
            } else if(klass == Map.class) {
                TypeVariable<Class<?>>[] typeVariable= (TypeVariable<Class<?>>[]) klass.getTypeParameters();
                Class<?> keyClass=typeVariable[0].getGenericDeclaration();
                Class<?> valueClass=typeVariable[0].getGenericDeclaration();
                
                String name=definitions.getName(index);
                Map value=row.getMap(index,keyClass,valueClass);
                record.set(name, value);
            }
        }
        return record;
    }

    private static StringBuilder transform(Tuple record) {
        StringBuilder builder=new StringBuilder("INSERT INTO ");
        //builder.append(Constant.SPACE);
        builder.append(record.getSchema())
                .append(Constant.DOT)
                .append(record.getTable())
                .append(Constant.SPACE)
                .append(Constant.LEFT_PARENTHESIS);
        
        StringBuilder valuesClause=new StringBuilder(" VALUES ");
        valuesClause.append(Constant.LEFT_PARENTHESIS);
        Map<String,Object> fields=record.getFields();
        Iterator<Map.Entry<String,Object>> iterator=fields.entrySet().iterator();
        while(iterator.hasNext()) {
            Map.Entry<String,Object> field=iterator.next();
            builder.append(field.getKey());
            Object value=field.getValue();
            
            //TODO need to process the values for appropriate type conversion
            if(value instanceof String || value instanceof  Date) {
                valuesClause.append(Constant.SINGLE_QUOTE);
                valuesClause.append(value);
                valuesClause.append(Constant.SINGLE_QUOTE);
            } else {
                valuesClause.append(value);
            }
            
            if(iterator.hasNext()) {
                builder.append(Constant.COMMA);
                valuesClause.append(Constant.COMMA);
            } else {
                builder.append(Constant.RIGHT_PARENTHESIS);
                valuesClause.append(Constant.RIGHT_PARENTHESIS);
            }
        }
        builder.append(valuesClause);
        return builder;
    }
    
    private static StringBuilder transformForUpdate(Tuple record) {
        StringBuilder builder=new StringBuilder("UPDATE ");
        builder.append(record.getSchema()).append(Constant.DOT);
        builder.append(record.getTable()).append(Constant.SPACE);
        builder.append("SET").append(Constant.SPACE);
        
        Map<String,Object> fields=record.getFields();
        Iterator<Map.Entry<String,Object>> iterator=fields.entrySet().iterator();
        while(iterator.hasNext()) {
            Map.Entry<String,Object> field=iterator.next();
            builder.append(field.getKey()).append(Constant.EQUALS);
            
            Object value=field.getValue();
            if(value instanceof String ||value instanceof Date) {
                builder.append(Constant.SINGLE_QUOTE);
                builder.append(value);
                builder.append(Constant.SINGLE_QUOTE);
            } else {
                builder.append(value);
            }
            if(iterator.hasNext()) {
                builder.append(Constant.COMMA).append(Constant.SPACE);
            } else {
                builder.append(Constant.SPACE);
            }
        }
        return builder;
    }
    
    public static BoundStatement setBoundValue(BoundStatement bs, int position, Object value) throws CassandraDatabaseException {
        if(value instanceof String) {
            bs.setString(position, (String)value);
        } else if(value instanceof Long) {
            bs.setLong(position, (Long)value);
        } else if(value instanceof ByteBuffer) {
            bs.setBytes(position, (ByteBuffer)value);
        } else if(value instanceof Boolean) {
            bs.setBool(position, (Boolean)value);
        } else if(value instanceof BigDecimal) {
            bs.setDecimal(position, (BigDecimal)value);
        } else if (value instanceof Double) {
            bs.setDouble(position, (Double)value);
        } else if (value instanceof Float) {
            bs.setFloat(position, (Float)value);
        } else if (value instanceof InetAddress) {
            bs.setInet(position, (InetAddress)value);
        } else if (value instanceof Integer) {
            bs.setInt(position, (Integer)value);
        } else if (value instanceof Date) {
            bs.setDate(position, (Date)value);
        } else if (value instanceof UUID) {
            bs.setUUID(position, (UUID)value);
        } else if (value instanceof BigInteger) {
            bs.setVarint(position, (BigInteger)value);
        } else if (value instanceof List) {
            bs.setList(position, (List)value);
        } else if (value instanceof Set) {
            bs.setSet(position, (Set)value);
        } else if (value instanceof Map) {
            bs.setMap(position, (Map)value);
        }else {
                throw new CassandraDatabaseException("Invalid bind parameter type : "+ value.getClass());
        }
        return bs;
    }

    public static BoundStatement setBoundValue(BoundStatement bs, String key, Object value) throws CassandraDatabaseException {
        if(value instanceof String) {
            bs.setString(key, (String)value);
        } else if(value instanceof Long) {
            bs.setLong(key, (Long)value);
        } else if(value instanceof ByteBuffer) {
            bs.setBytes(key, (ByteBuffer)value);
        } else if(value instanceof Boolean) {
            bs.setBool(key, (Boolean)value);
        } else if(value instanceof BigDecimal) {
            bs.setDecimal(key, (BigDecimal)value);
        } else if (value instanceof Double) {
            bs.setDouble(key, (Double)value);
        } else if (value instanceof Float) {
            bs.setFloat(key, (Float)value);
        } else if (value instanceof InetAddress) {
            bs.setInet(key, (InetAddress)value);
        } else if (value instanceof Integer) {
            bs.setInt(key, (Integer)value);
        } else if (value instanceof Date) {
            bs.setDate(key, (Date)value);
        } else if (value instanceof UUID) {
            bs.setUUID(key, (UUID)value);
        } else if (value instanceof BigInteger) {
            bs.setVarint(key, (BigInteger)value);
        } else if (value instanceof List) {
            bs.setList(key, (List)value);
        } else if (value instanceof Set) {
            bs.setSet(key, (Set)value);
        } else if (value instanceof Map) {
            bs.setMap(key, (Map)value);
        }else {
                throw new CassandraDatabaseException("Invalid bind parameter type : "+ value.getClass());
        }
        return bs;
    }
    
    public static Object getValue(Row row, String name, DataType type) throws CassandraDatabaseException {
        switch (type.getName()) {
            case ASCII:
                return row.getString(name);
            case BIGINT:
                return row.getLong(name);
            case BLOB:
                return row.getBytes(name);
            case BOOLEAN:
                return row.getBool(name);
            case COUNTER:
                return row.getLong(name);
            case DECIMAL:
                return row.getDecimal(name);
            case DOUBLE:
                return row.getDouble(name);
            case FLOAT:
                return row.getFloat(name);
            case INET:
                return row.getInet(name);
            case INT:
                return row.getInt(name);
            case TEXT:
                return row.getString(name);
            case TIMESTAMP:
                return row.getDate(name);
            case UUID:
                return row.getUUID(name);
            case VARCHAR:
                return row.getString(name);
            case VARINT:
                return row.getVarint(name);
            case TIMEUUID:
                return row.getUUID(name);
            case LIST:
                return row.getList(name, classOf(type.getTypeArguments().get(0)));
            case SET:
                return row.getSet(name, classOf(type.getTypeArguments().get(0)));
            case MAP:
                return row.getMap(name, classOf(type.getTypeArguments().get(0)), classOf(type.getTypeArguments().get(1)));
        }
        throw new CassandraDatabaseException("Missing handling of " + type);
    }

    private static Class classOf(DataType type) throws CassandraDatabaseException {
        assert !type.isCollection();

        switch (type.getName()) {
            case ASCII:
            case TEXT:
            case VARCHAR:
                return String.class;
            case BIGINT:
            case COUNTER:
                return Long.class;
            case BLOB:
                return ByteBuffer.class;
            case BOOLEAN:
                return Boolean.class;
            case DECIMAL:
                return BigDecimal.class;
            case DOUBLE:
                return Double.class;
            case FLOAT:
                return Float.class;
            case INET:
                return InetAddress.class;
            case INT:
                return Integer.class;
            case TIMESTAMP:
                return Date.class;
            case UUID:
            case TIMEUUID:
                return UUID.class;
            case VARINT:
                return BigInteger.class;
        }
        throw new CassandraDatabaseException("Missing handling of " + type);
    }
    
    public static Date getDate() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
}
