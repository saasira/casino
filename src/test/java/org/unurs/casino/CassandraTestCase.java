/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.unurs.casino;

import com.datastax.driver.core.CloseFuture;
import com.datastax.driver.core.DataType;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.core.Session;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.unurs.linker.Stringer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author redu
 */
//@FixMethodOrder(MethodSorters.JVM)
public class CassandraTestCase {
    
    private static final String NAME_COLUMN="name";
    private static final String LAST_MODIFIED_COLUMN="last_modified_at";
    
    public CassandraTestCase() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        CassandraSettings.setClusterName("rest");
        Set<String> nodes= new HashSet<>();
        nodes.add("127.0.0.1");
        CassandraSettings.setClusterNodes(nodes);
        CassandraSettings.setClusterPort(9042); 
        Cassandra.connect();
        assertNotNull( Cassandra.getClusterMetadata());
    }
    
    @AfterClass
    public static void tearDownClass() {
        
        //Cassandra.dropTable("test", "best");
        
        CloseFuture future=Cassandra.closeAsync();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(CassandraTestCase.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertTrue(future.isDone());
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
        
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testSession() {
        Session session=Cassandra.getSession();
        assertNotNull(session);
    }
    
    @Test
    public void testCreateSchema() {
        Cassandra.createSchema("test","SimpleStrategy", 1, true);
    }
    
    @Test
    public void testCreateTable() {
        Table table=new Table("test","best");
        //table.addColumn(new Column("uuid",DataType.uuid()));
        table.addColumn(new Column(NAME_COLUMN,DataType.text()));
        table.addColumn(new Column("value",DataType.blob()));
        table.addColumn(new Column("size",DataType.bigint()));
        table.addColumn(new Column(LAST_MODIFIED_COLUMN,DataType.timestamp()));
        table.addColumn(new Column("deleted",DataType.cboolean()));
        List<String> pk=new ArrayList<>();
        pk.add(NAME_COLUMN);
        //pk.add(LAST_MODIFIED_COLUMN);
        table.setPrimaryKey(pk);
        /*Map<String,String> indices=new HashMap();
        indices.put("test_best_last_modified_at_index",LAST_MODIFIED_COLUMN);
        table.setIndices(indices);
        */
        Cassandra.createTable(table);
        try {
            assertTrue(Cassandra.doesTableExist("test","best"));
        } catch (CassandraDatabaseException ex) {
            Logger.getLogger(CassandraTestCase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Test//not using; instead we make it part of primary key
    public void testCreateIndex() {
        ResultSetFuture rsf=Cassandra.createIndex("test", "best", "test_best_last_modified_at_index",LAST_MODIFIED_COLUMN);
        rsf.getUninterruptibly();
        assertTrue(rsf.isDone());
        
        rsf=Cassandra.createIndex("test", "best", "test_best_deleted_index","deleted");
        rsf.getUninterruptibly();
        assertTrue(rsf.isDone());
    }
    
    @Test
    public void testInsertSingleRecord() {
        try {
            assertTrue(Cassandra.doesTableExist("test","best"));
        } catch (CassandraDatabaseException ex) {
            Logger.getLogger(CassandraTestCase.class.getName()).log(Level.SEVERE, null, ex);
        }
        Tuple record=new Tuple("test","best");
        UUID uuid=UUID.randomUUID();
        String name=Stringer.random(16);
        
        //record.set("uuid",uuid);
        record.set("name",name);
        record.set("size", 2048);
        record.set(LAST_MODIFIED_COLUMN,getDate().getTimeInMillis());
        
        //record.set("value", null);
        record.set("deleted",false);
        Cassandra.insert(record);
        assertNotNull(Cassandra.find("test","best", new Expression("name",name,Operator.eq())));
    }
    
    @Test
    public void testInsertMultipleRecords() {
        List<Tuple> data=new ArrayList<>();
        Tuple record=new Tuple("test","best");
        //UUID uuid=UUID.randomUUID();
        String name=Stringer.random(16);
        //record.set("uuid",uuid);
        record.set("name",name);
        record.set(LAST_MODIFIED_COLUMN, getDate().getTimeInMillis());
        record.set("size",1024);
        record.set("deleted",false);
        data.add(record);
        
        record=new Tuple("test","best");
        //uuid=UUID.randomUUID();
        name=Stringer.random(16);
        //record.set("uuid",uuid);
        record.set("name",name);
        record.set(LAST_MODIFIED_COLUMN, getDate().getTimeInMillis());
        record.set("size",3072);
        data.add(record);
        
        record=new Tuple("test","best");
        //uuid=UUID.randomUUID();
        name=Stringer.random(16);
        //record.set("uuid",uuid);
        record.set("name",name);
        record.set(LAST_MODIFIED_COLUMN, getDate().getTimeInMillis());
        record.set("size",4096);
        data.add(record);
        
        record=new Tuple("test","best");
        //uuid=UUID.randomUUID();
        name=Stringer.random(16);
        //record.set("uuid",uuid);
        record.set("name",name);
        record.set(LAST_MODIFIED_COLUMN, getDate().getTimeInMillis());
        record.set("size",5102);
        data.add(record);
        
        record=new Tuple("test","best");
        //uuid=UUID.randomUUID();
        name=Stringer.random(16);
        //record.set("uuid",uuid);
        record.set("name",name);
        record.set(LAST_MODIFIED_COLUMN, getDate().getTimeInMillis());
        record.set("size",6144);
        //record.set("value", ByteBuffer.allocate(4096));
        record.set("deleted",false);
        data.add(record);
        
        record=new Tuple("test","best");
        //uuid=UUID.randomUUID();
        //record.set("uuid",uuid);
        record.set("name","Samba");
        record.set(LAST_MODIFIED_COLUMN, getDate().getTimeInMillis());
        record.set("size",7168);
        //record.set("value", ByteBuffer.allocate(4096));
        record.set("deleted",false);
        data.add(record);
        
        record=new Tuple("test","best");
        record.set("name","saasira");
        record.set(LAST_MODIFIED_COLUMN, getDate().getTimeInMillis());
        record.set("size",7168);
        record.set("deleted",false);
        data.add(record);
        
        Cassandra.insert(data);
    }
    
    //@Test // works but this record was not insereted earlier 
    public void testFindRecordById() {
        assertNotNull(Cassandra.find("test","best", new Expression("name","saasira",Operator.eq())));
    }
    
    @Test
    public void testFindRecordByOtherField() {
        LinkedList<Expression> expressions=new LinkedList<>();
        Criteria criteria=Criteria.instance("test", "best")
                .fetch("*").where(new Expression("deleted",true,Operator.eq()))
                .and(new Expression(LAST_MODIFIED_COLUMN,getDate().getTimeInMillis(),Operator.lte()));
        assertNotNull(Cassandra.find("test","best",criteria.getExpressions(),true));
    }
    
    //@Test //does not work
    public void testDeleteByOtherField() {
        String query="DELETE FROM test.best where deleted=true AND "+LAST_MODIFIED_COLUMN+"=?;";
        String queryName="delete_by_other_field";
        List<Object> bp=new ArrayList<>();
        bp.add(Cassandra.getDate());
        try {
            Cassandra.prepareQuery(queryName, query);
            Cassandra.executePreparedCommand(queryName, bp);
        } catch (CassandraDatabaseException ex) {
            Logger.getLogger(CassandraTestCase.class.getName()).log(Level.SEVERE, null, ex);
            fail("failed to delete by no primary key column(s) in where clause");
        }
    }
    
    //@Test // works but the data inserted earlier does not contain this record
    public void testQuerySingle() {
        String query="SELECT * FROM test.best WHERE name='saasira';";
        Tuple tuple=Cassandra.querySingle(query);
        assertNotNull(tuple);
    }
    
    @Test
    public void testQueryMultiple() {
        String query="SELECT * FROM test.best;";
        List<Tuple> tuples=Cassandra.queryMultiple(query);
        assertNotNull(tuples);
        assertTrue(!tuples.isEmpty());
    }
    
    @Test
    public void testUpdateSingleRecordWithSingleIdentifier(){
        Tuple record=new Tuple("test","best");
        //UUID uuid=UUID.fromString("898d1b32-fc3d-454d-8a69-0f3e7aa47a29");
        String name=Stringer.random(16);
        //record.set("uuid",uuid);
        //record.set("name",name);
        //record.set("value",ByteBuffer.allocate(4096));
        record.set("size",10240);
        record.set(LAST_MODIFIED_COLUMN,getDate().getTimeInMillis());
        record.set("deleted",false);
        Cassandra.update(record, "name","Samba");
    }
    
    @Test
    public void testUpdateSingleRecordWithMultipleIdentifiers(){
        Tuple record=new Tuple("test","best");
        //UUID uuid=UUID.fromString("898d1b32-fc3d-454d-8a69-0f3e7aa47a29");
        String name="saasira";
        //record.set("uuid",uuid);
        Map<String,Object> identifiers=new HashMap<>();
        //identifiers.put("uuid", uuid);
        identifiers.put("name",name);
        //identifiers.put("deleted",true);
        //identifiers.put(LAST_MODIFIED_COLUMN,getDate().getTimeInMillis());
        //record.set("name",Stringer.random(16));
        //record.set("value",ByteBuffer.allocate(4096));
        record.set("size",10240);
        record.set(LAST_MODIFIED_COLUMN,getDate().getTimeInMillis());
        record.set("deleted",false);
        
        Cassandra.update(record, identifiers);
    }
    
    //@Test //works but the data inserted earlier does not contain this name
    public void testQuerySinglePrepared() {
        String queryName="get_single_by_name";
        String cql="SELECT * FROM test.best where name=?;";
        List<Object> bp=new ArrayList<>();
        bp.add("saasira");
        
        Tuple tuple=null;
        try {
            Cassandra.prepareQuery(queryName, cql);
            tuple = Cassandra.querySingle(queryName,bp);
        } catch (CassandraDatabaseException ex) {
            Logger.getLogger(CassandraTestCase.class.getName()).log(Level.SEVERE, null, ex);
            fail("error executing prepared query with bind parameters");
        }
        assertNotNull(tuple);
    }
    
    @Test
    public void testQueryMultiplePrepared() {
        String queryName="get_all_by_name";
        String cql="SELECT * FROM test.best;";
        
        List<Tuple> tuples=null;
        try {
            Cassandra.prepareQuery(queryName, cql);
            tuples = Cassandra.queryMultiple(queryName,((List<Object>)null));
        } catch (CassandraDatabaseException ex) {
            Logger.getLogger(CassandraTestCase.class.getName()).log(Level.SEVERE, null, ex);
            fail("error executing prepared query with bind parameters");
        }
        assertNotNull(tuples);
        assertTrue(!tuples.isEmpty());
    }
    
    @Test
    public void testExecutingPreparedCommand() {
        String queryName="intert_data";
        String query="INSERT INTO test.best (name,value,size,last_modified_at,deleted) VALUES (?,?,?,?,?)";
        List<Object> bp=new ArrayList<>();
        ByteBuffer buffer=ByteBuffer.allocate(1024*1024);
        for(int index=0;index<4096;index++) {
            buffer.put(new Integer(index).byteValue());
        }
        buffer.limit(buffer.position());
        //bp.add(UUID.randomUUID());
        String name="stupidity";//Stringer.random(16);
        bp.add(name);
        bp.add(buffer);
        bp.add((long)buffer.limit());
        bp.add(getDate().getTime());
        bp.add(true);
        
        boolean success=false;
        try {
            Cassandra.prepareQuery(queryName, query);
            Cassandra.executePreparedCommand(queryName, bp);
            success=true;
        } catch (CassandraDatabaseException ex) {
            Logger.getLogger(CassandraTestCase.class.getName()).log(Level.SEVERE, null, ex);
            fail("error executing a prepared insert command");
        }
        assertTrue(success);
    }
    
    @Test
    public void testPreparedInsertIfNotPresent() {
        String queryName="intert_data_if not present";
        String query="INSERT INTO test.best (name,value,size,last_modified_at,deleted) VALUES (?,?,?,?,?) IF NOT EXISTS;";
        List<Object> bp=new ArrayList<>();
        ByteBuffer buffer=ByteBuffer.allocate(4096);
        for(int index=0;index<4096;index++) {
            buffer.put(new Integer(index).byteValue());
        }
        //bp.add(UUID.randomUUID());
        String name="writer.lock";//Stringer.random(16);
        bp.add(name);
        bp.add(buffer);
        bp.add(20480L);
        bp.add(getDate().getTime());
        bp.add(false);
        
        boolean success=false;
        try {
            Cassandra.prepareQuery(queryName, query);
            Cassandra.executePreparedCommand(queryName, bp);
            success=true;
        } catch (CassandraDatabaseException ex) {
            Logger.getLogger(CassandraTestCase.class.getName()).log(Level.SEVERE, null, ex);
            fail("error executing a prepared insert command");
        }
        assertTrue(success);
    }
    
    
    @Test
    public void testPreparedDeleteByName() {
        String queryName="delete_by_name";
        String query="DELETE FROM test.best WHERE name=?;";
        List<Object> bp=new ArrayList<>();
        ByteBuffer buffer=ByteBuffer.allocate(4096);
        for(int index=0;index<4096;index++) {
            buffer.put(new Integer(index).byteValue());
        }
        //bp.add(UUID.randomUUID());
        String name="writer.lock";//Stringer.random(16);
        bp.add(name);

        
        boolean success=false;
        try {
            Cassandra.prepareQuery(queryName, query);
            Cassandra.executePreparedCommand(queryName, bp);
            success=true;
        } catch (CassandraDatabaseException ex) {
            Logger.getLogger(CassandraTestCase.class.getName()).log(Level.SEVERE, null, ex);
            fail("error executing a prepared insert command");
        }
        assertTrue(success);
    }
    
    @Test
    public void testDropTable() {
        Cassandra.dropTable("test", "best");
    }
    
    public static Calendar getDate() {
        Date dateWithoutTime;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        //dateWithoutTime = calendar.getTime();
        //dateFormat.format(dateWithoutTime);
        //System.out.println(dateWithoutTime);
        //System.out.println(calendar.getTimeZone()); 
        return calendar;
    }
}
