/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.unurs.casino;

import com.datastax.driver.core.DataType;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.CloseFuture;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author redu
 */
public class AnotherTestCase {
    
    private static final String CLUSTER="search";
    private static final String SCHEMA="lucene";
    private static final String TABLE="posting";
    
    private static final String DOCUMENT_NUMBER_COLUMN="document_number";
    private static final String FIELD_NUMBER_COLUMN="field_number";
    private static final String FIELD_NAME_COLUMN="field_name";
    private static final String TERM_COLUMN="term";
    private static final String SEGMENT_COLUMN="segment";
    private static final String FREQUENCY_COLUMN="frequency";
    
    public AnotherTestCase() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        CassandraSettings.setClusterName(CLUSTER);
        Set<String> nodes= new HashSet<>();
        nodes.add("127.0.0.1");
        CassandraSettings.setClusterNodes(nodes);
        CassandraSettings.setClusterPort(9042); 
        Cassandra.connect();
        assertNotNull( Cassandra.getClusterMetadata());
    }
    
    @AfterClass
    public static void tearDownClass() {
        //Cassandra.dropTable(SCHEMA, TABLE);
        
        CloseFuture future=Cassandra.closeAsync();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(CassandraTestCase.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertTrue(future.isDone());
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSession() {
        Session session=Cassandra.getSession();
        assertNotNull(session);
    }
    
    @Test
    public void testCreateSchema() {
        Cassandra.createSchema(SCHEMA,"SimpleStrategy", 1, true);
    }
    
    @Test
    public void testCreateTable() {
        Table table=new Table(SCHEMA,TABLE);
        //table.addColumn(new Column("uuid",DataType.uuid()));
        table.addColumn(new Column(DOCUMENT_NUMBER_COLUMN,DataType.bigint()));
        table.addColumn(new Column(FIELD_NUMBER_COLUMN,DataType.bigint()));
        table.addColumn(new Column(FIELD_NAME_COLUMN,DataType.text()));
        table.addColumn(new Column(TERM_COLUMN,DataType.text()));
        table.addColumn(new Column(SEGMENT_COLUMN,DataType.text()));
        table.addColumn(new Column(FREQUENCY_COLUMN,DataType.cint()));
        List<String> pk=new ArrayList<>();
        pk.add(DOCUMENT_NUMBER_COLUMN);
        //pk.add(FIELD_NUMBER_COLUMN);
        pk.add(TERM_COLUMN);
        table.setPrimaryKey(pk);
        /*Map<String,String> indices=new HashMap();
        indices.put("test_best_last_modified_at_index",LAST_MODIFIED_COLUMN);
        table.setIndices(indices);
        */
        Cassandra.createTable(table);
        try {
            assertTrue(Cassandra.doesTableExist(SCHEMA,TABLE));
        } catch (CassandraDatabaseException ex) {
            Logger.getLogger(CassandraTestCase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Test//not using; instead we make it part of primary key
    public void testCreateIndex() {
        ResultSetFuture rsf=Cassandra.createIndex(SCHEMA, TABLE, "search_lucene_field_name_index",FIELD_NAME_COLUMN);
        rsf.getUninterruptibly();
        assertTrue(rsf.isDone());
        
        rsf=Cassandra.createIndex(SCHEMA, TABLE, "search_lucene_segment_index", SEGMENT_COLUMN);
        rsf.getUninterruptibly();
        assertTrue(rsf.isDone());
    }
    
    @Test
    public void testGetByTermSorted() {
    
    }
}
